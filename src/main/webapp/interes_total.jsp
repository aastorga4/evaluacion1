<%-- 
    Document   : interes_total
    Created on : 11-04-2021, 23:02:45
    Author     : Alonso
--%>

<%@page import="root.model.CalcularInteres"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  CalcularInteres interes=(CalcularInteres)request.getAttribute("interes");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Tu interes simple es de: <%= interes.getTotal()%> </h1>
    </body>
</html>
