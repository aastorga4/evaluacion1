/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

/**
 *
 * @author Alonso
 */
public class CalcularInteres {
  private int c, i,  n, total;
    
    public int getTotal(){
       return total; 
    }
    
     public void setTotal(int total){
       this.total= total; 
    }
    /**
     * @return the c
     */
    public int getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(int c) {
        this.c = c;
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }

    /**
     * @return the n
     */
    public int getN() {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(int n) {
        this.n = n;
    }
    public void calculoInteres(int c,int i,int n){
        this.total= c * (i/100) * n;
    
    }
    
}
